/**
 * Created by lli on 24.03.2017.
 *
 *
 */
var language = 'en';
$(document).ready(function () {
    /*    var accepted = Cookies.get('sparql-manager-cookies-accepted');
        if (accepted === "true"){
            $("#cookie-consent").remove();
            console.log("we found it")
        }*/
    var myRe = new RegExp('/.{2}/', 'g');
    var myArray = myRe.exec(window.location.href);
    language = myArray[0].substring(1, 3);
    document.getElementById('language-selector3').value = language;
    init();
});
var init = function () {
    jQuery.i18n.properties({
            name: 'indextranslations',
            path: window.location.origin + "/sparql-manager/resources/languages/new/",
            mode: 'both',
            language: language,
            checkAvailableLanguages: false,
            async: false,
            callback: function () {
                const translationId = 85;
                let count;
                for (count = translationId; count >= 0; count--) {
                    let element = document.getElementById('text' + count);

                    if (element == null) {
                        continue;
                    }

                    let translation = jQuery.i18n.prop(element.dataset.title);

                    switch (count) {
                        case 60:
                            element.innerText = translation;
                            break;
                        case 69:
                            element.innerHTML = translation;
                            break;
                        case 54:
                            element.value = translation;
                            break;
/*
                        case 9:
                            element.placeholder = translation;
                            break;
 */
                        default:
                            element.innerText = translation;
                    }
                 }

                const languageSelect = document.getElementsByClassName('languageSelect');
                for (let element of languageSelect) {
                    if (element.href.match("mqa") || element.href.match("catalogue-statistics")) {
                        element.href = element.href.concat("?locale=" + language);
                    }
                    if (element.href.match("/data/datasets") || element.href.match("/data/eu-international-dataset")) {
                        element.href = element.href.concat("?locale=" + language);
                    }
                    element.href = element.href.replace(/en/i, language);
                }
            }
        }
    );
};

var changelanguage = function (value) {
    window.location = window.location.href.replace(/\/.{2}\//, "/" + value + "/");
    language = value;
};

var whichSearch = true;
var goSearch = function () {
    if (whichSearch)
        window.location = window.location.origin + "/" + language + "/search?term=" + document.getElementById('text9').value + "&searchdomain=site";
    else
        window.location = window.location.origin + "/data/datasets?query=" + document.getElementById('text9').value + "&locale=" + language;
};

var changeSearch = function (value) {
    if (value === "site")
        whichSearch = true;
    else
        whichSearch = false;
};

var hideConsens = function (accepted) {
    $("#cookie-consent").slideUp('slow');
    if (accepted) {
        Cookies.set('sparql-manager-cookies-accepted', 'true')
    }
};
