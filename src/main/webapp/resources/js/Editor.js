/**
 * Created by lli on 02.11.2016.
 */
var textarea = $("#transformation > textarea").hide();
var editor = ace.edit("editor");
editor.setTheme("ace/theme/chrome");
editor.getSession().setMode("ace/mode/sql");
editor.getSession().setValue(textarea.val());
editor.getSession().on('change', function () {
    textarea.val(editor.getSession().getValue());
});