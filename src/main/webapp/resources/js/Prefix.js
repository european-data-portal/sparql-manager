/**
 * Created by lli on 07.11.2016.
 */
var prefix = ace.edit("prefix");
prefix.setTheme("ace/theme/chrome");
prefix.getSession().setMode(
    "ace/mode/sql");
prefix.setReadOnly(true);