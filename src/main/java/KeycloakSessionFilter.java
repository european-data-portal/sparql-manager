import de.fhg.fokus.edp.sparqlmanager.managedbeans.ProfileBean;
import de.fhg.fokus.edp.sparqlmanager.service.CrudServiceBean;
import de.fhg.fokus.edp.sparqlmanager.service.model.User;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class KeycloakSessionFilter implements Filter {

    @Inject
    private ProfileBean profileBean;

    @Inject
    private Logger logger;

    @Inject
    private CrudServiceBean crudServiceBean;


    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        if (!profileBean.isAuthenticated()) {

            HttpServletRequest request = (HttpServletRequest) req;
            RefreshableKeycloakSecurityContext context
                    = (RefreshableKeycloakSecurityContext) request.getAttribute("org.keycloak.KeycloakSecurityContext");
            //or (RefreshableKeycloakSecurityContext)request.getSession().getAttribute("org.keycloak.KeycloakSecurityContext");
            AccessToken token = context.getToken();


/*
            System.out.println(token.getId());
*/

            profileBean.setUserProfile(token.getProfile());
            profileBean.setUserId(token.getId());


            String userMail = token.getEmail();
            if (profileBean.getUserMail() == null || profileBean.getUserMail().isEmpty())
                profileBean.setUserMail(profileBean.getUserId() + "@example.com");
            else profileBean.setUserMail(userMail);

            String loginName = token.getPreferredUsername();
            if (loginName == null || loginName.isEmpty())
                profileBean.setDomainUsername(token.getId());
            else profileBean.setDomainUsername(loginName);

            String firstName = token.getName();
            if (firstName == null || firstName.isEmpty())
                profileBean.setFirstName(token.getId());
            else profileBean.setFirstName(firstName);

            String lastName = token.getFamilyName();
            if (lastName == null || lastName.isEmpty())
                profileBean.setLastName(token.getId());
            else profileBean.setLastName(lastName);

            String locale = token.getLocale();
            if (locale == null || locale.isEmpty())
                profileBean.setLocale("en");
            else profileBean.setLocale(locale);

            User u = crudServiceBean.getUser(profileBean.getUserId());
            if (u == null) {
                logger.debug("autocreation of user '{}'", profileBean.getUserId());
                u = new User();
                u.setUserId(profileBean.getUserId());
                crudServiceBean.addUser(u);
            }

            profileBean.setAuthenticated(!token.isExpired());


            request.getSession().setAttribute("loginName", loginName);
        }
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
