package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import de.fhg.fokus.edp.sparqlmanager.messages.MessageProvider;
import de.fhg.fokus.edp.sparqlmanager.scheduler.PeriodicTimer;
import de.fhg.fokus.edp.sparqlmanager.service.CrudServiceBean;
import de.fhg.fokus.edp.sparqlmanager.service.jena.SparqlClient;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;
import de.fhg.fokus.edp.sparqlmanager.service.model.User;
import org.ocpsoft.rewrite.faces.navigate.Navigate;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Named
@ViewScoped
public class QueryBean implements Serializable {

	@Inject
	private CrudServiceBean crudServiceBean;

	@EJB
	private PeriodicTimer periodicTimer;

	@Inject
	private SearchBean searchbean;


	@Inject
	FacesMessageBean facesMessageBean;

	@Inject
	LanguageBean languageBean;

	@Inject
	private transient Logger logger;

	@Inject
	private ProfileBean profileBean;

	@Inject
	SparqlClient sparqlClient;

	@Inject
	private MessageProvider messageProvider;

	private long queryId;

	private Query query = null;

	private Date timeout = null;

	private User owner = null;

	private boolean authenticated = false;

	private List<Result> results;

	private static final long serialVersionUID = 1L;

	public Navigate loadQuery() {
		logger.debug("queryId {}", queryId);

		for (Query q : crudServiceBean.getAllQueries()) {
			if(q.getQueryId() == queryId){

				query = q;
				break;
			}
		}

		if(profileBean.getUserId()!=null) {
			User user = crudServiceBean.getUser(profileBean.getUserId());
/*			User user = crudServiceBean.getUser(profileBean.getUserId());
			for (Query q : user.getQueries()) {
				if (q.getQueryId() == queryId) {
					query = q;
					break;
				}
			}*/
			for (User u : crudServiceBean.getUsers()){
				if(u.getQueries().contains(query)){
					owner = u;
					owner.getUserId();
					user.getUserId();
					if(profileBean.getUserId()!= null && owner.getUserId()==user.getUserId()){
						authenticated =true;
					}
					break;
				}
			}

		}


		if (query != null) {
			timeout = periodicTimer.getTimeout(query);
		} else {
			authenticated = true;
			Date now = new Date();
			query = new Query();
			query.setQueryString(searchbean.getQuery().getQueryString());
			//query.setQueryName("query from " + now);
			//query.setComment("query comment from " + now);
			query.setNotificationString(profileBean.getUserMail());
			query.setResultFormat(searchbean.getQuery().getResultFormat());
		}
		if(isAuthenticated()==false && query.isPublicised()==false){

			facesMessageBean.addMessage(FacesMessage.SEVERITY_INFO, null,
					messageProvider.getValue("SPARQLMANAGER.MESSAGE.SEARCH_ERROR"));

			return Navigate.to("/" + languageBean.getLang().toString() + "/queries");
		}
			return null;

	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public Navigate createQuery() {
		String userId = profileBean.getUserId();
		crudServiceBean.addQuery(userId, query);

		return Navigate.to("/" + languageBean.getLang().toString() + "/queries");
	}

	public Navigate updateQuery() {

		crudServiceBean.editQuery(query);

		facesMessageBean.addMessage(FacesMessage.SEVERITY_INFO, null,
				messageProvider.getValue("SPARQLMANAGER.MESSAGE.QUERY_UPDATED"));

		return Navigate.to("/" + languageBean.getLang().toString() + "/query/" + queryId);
	}

	public Navigate deleteQuery() {
		Boolean success = crudServiceBean.deleteQuery(profileBean.getUserId(), this.queryId);
		if (success)
			facesMessageBean.addMessage(FacesMessage.SEVERITY_INFO, null,
					messageProvider.getValue("SPARQLMANAGER.MESSAGE.QUERY_DELETED"));
		else
			facesMessageBean.addMessage(FacesMessage.SEVERITY_ERROR, null,
					messageProvider.getValue("SPARQLMANAGER.MESSAGE.QUERY_NOT_DELETED"));

		return Navigate.to("/" + languageBean.getLang().toString() + "/queries");
	}

	public List<Result> getResults() {
		results = crudServiceBean.getResults(queryId);
		return results;
	}
	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	public Long getQueryId() {
		return queryId;
	}

	public void setQueryId(Long queryId) {
		this.queryId = queryId;
	}

	public void validateScheduleString(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		String scheduleString = (String) value;

		// TODO validateScheduleString
	}

	public void validateSparqlQuery(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		String sparqlQuery = (String) value;
		// TODO validateSparqlQuery
		// try {
		// sparqlClient.runQuery(sparqlQuery);
		// } catch (Exception e) {
		// throw new ValidatorException(new
		// FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
		// e.getMessage()));
		// }
	}

	public Date getTimeout() {
		return timeout;
	}

	public void setTimeout(Date timeout) {
		this.timeout = timeout;
	}

	public Navigate executeQuery() {
		periodicTimer.timeout(queryId);

		facesMessageBean.addMessage(FacesMessage.SEVERITY_INFO, null,
				messageProvider.getValue("SPARQLMANAGER.MESSAGE.QUERY_EXECUTED"));

		return Navigate.to("/" + languageBean.getLang().toString() + "/query/" + queryId);
	}
}