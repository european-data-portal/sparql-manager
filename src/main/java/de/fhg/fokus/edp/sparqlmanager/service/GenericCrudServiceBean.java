package de.fhg.fokus.edp.sparqlmanager.service;

import org.slf4j.Logger;

import javax.ejb.Stateful;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

@ApplicationScoped
@Stateful
//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class GenericCrudServiceBean implements GenericCrudService {

	@Inject
	private transient Logger logger;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	// @PersistenceContext
	private EntityManager em;

	@Transactional
	public Serializable create(Serializable t) {
		logger.debug("create({})", t);
		this.em.persist(t);
		this.em.flush();
		return t;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Serializable find(Serializable id, Class type) {
		logger.debug("find({},{})", id, type);
		return (Serializable) this.em.find(type, id);
	}

	@Transactional
	public void delete(Serializable t) {
		logger.debug("delete({})", t);
		t = this.em.merge(t);
		this.em.remove(t);
		em.flush();
	}

	public void delete(Serializable id, Class type) {
		logger.debug("delete({},{})", id, type);
		delete(find(id, type));
	}

	@Transactional
	public Serializable update(Serializable t) {
		logger.debug("update({})", t);
		return this.em.merge(t);
	}


	public Collection<Serializable> findByNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	public int countNamedQuery(String namedQueryName){
		return ((Long)this.em.createNamedQuery(namedQueryName).getSingleResult()).intValue();
	}


	public Collection<Serializable> findByNamedQuery(String namedQueryName, int resultLimit, int firstLimit) {
		return this.em.createNamedQuery(namedQueryName).setMaxResults(resultLimit).setFirstResult(firstLimit).getResultList();
	}


	public int countNamedQuery(String namedQueryName, String userId){
		return (int)this.em.createNamedQuery(namedQueryName).setParameter("id", userId).getSingleResult();
	}


	public Collection<Serializable> findByNamedQuery(String namedQueryName, int resultLimit, int firstLimit, String userId) {
		return this.em.createNamedQuery(namedQueryName).setParameter("id", userId).setMaxResults(resultLimit).setFirstResult(firstLimit).getResultList();
	}


	public Collection<Serializable> findByNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		return findByNamedQuery(namedQueryName, parameters, 0);
	}


	public Collection<Serializable> findByNamedQuery(String namedQueryName, Map<String, Object> parameters,
			int resultLimit) {
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0) {
			query.setMaxResults(resultLimit);
		}
		for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}
}