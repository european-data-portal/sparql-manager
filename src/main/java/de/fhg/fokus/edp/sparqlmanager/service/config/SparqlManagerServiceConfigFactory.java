package de.fhg.fokus.edp.sparqlmanager.service.config;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class SparqlManagerServiceConfigFactory {

	@Inject
	private transient Logger logger;

	@Inject
	private SystemConfiguration systemConfiguration;

	@Produces
	@SparqlManagerServiceConfig(value = "", defaultValue = "")
	public String createConfigValue(InjectionPoint injectionPoint) {
		String key = injectionPoint.getAnnotated()
				.getAnnotation(SparqlManagerServiceConfig.class).value();
		String defaultKey = injectionPoint.getAnnotated()
				.getAnnotation(SparqlManagerServiceConfig.class).defaultValue();

		String value;

		if (key.isEmpty()) {
			value = systemConfiguration.getProperties().getProperty(defaultKey);
			logger.warn(
					"Value for key {} is empty. Returning default value: {}",
					key, value);
		} else {
			value = systemConfiguration.getProperties().getProperty(key);
		}

		return value;
	}
}
