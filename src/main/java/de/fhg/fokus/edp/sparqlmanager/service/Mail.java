package de.fhg.fokus.edp.sparqlmanager.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import de.fhg.fokus.edp.sparqlmanager.managedbeans.ProfileBean;
import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;

@Stateless
public class Mail {

	@Inject
	private transient Logger logger;

	@Resource(name = "java:jboss/mail/sparql-manager")
	private Session session;

	@Inject
	private ProfileBean profileBean;

	private TemplateEngine templateEngine;

	@Inject
	SystemConfiguration systemConfiguration;

	private String REPLY_EMAIL;

	@PostConstruct
	public void init() {
		REPLY_EMAIL = systemConfiguration.getProperties().getProperty(SparqlManagerServiceConfigKeys.REPLY_EMAIL);

		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setTemplateMode("HTML");
		templateResolver.setPrefix("/emails/");
		templateResolver.setSuffix(".html");

		templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
	}

	@Asynchronous
	public void send(String addresses, String subject, String text) {
		if (session == null || addresses == null || addresses.isEmpty()) {
			return;
		}

		Context ctx = new Context();
		// ctx.setVariables(parameters);
		// ctx.setVariable("firstname", user.getFirstName());
		// ctx.setVariable("lastname", user.getLastName());

		ctx.setVariable("logo", "logo");
		ctx.setVariable("title", subject);
		ctx.setVariable("text", text);

		String content = templateEngine.process("simple", ctx);
		logger.info("email: {}", content);

		try {
			Message message = new MimeMessage(session);
			//TODO use firstname and lastname in email
			// String personal = user.getFirstName();
			// if (user.getLastName() != null && !user.getLastName().isEmpty())
			// {
			// personal = String.join(" ", personal, user.getLastName());
			// }
			// Address address = null;
			// if (personal != null && !personal.isEmpty()) {
			// try {
			// address = new InternetAddress(user.getEmail(), personal);
			// } catch (UnsupportedEncodingException e) {
			// logger.error("address invalid", e);
			// }
			// } else {
			// address = new InternetAddress(user.getEmail());
			// }
			message.setFrom(new InternetAddress(REPLY_EMAIL));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(addresses));
			message.setSubject(subject);

			MimeMultipart multipart = new MimeMultipart("related");

			MimeBodyPart html = new MimeBodyPart();
			html.setContent(content, "text/html; charset=utf-8");
			multipart.addBodyPart(html);

			try {
				DataHandler handler = new DataHandler(new ByteArrayDataSource(Thread.currentThread()
						.getContextClassLoader().getResourceAsStream("/emails/EDP_LOGO_SMALL.png"), "image/png"));
				MimeBodyPart logo = new MimeBodyPart();
				logo.setDataHandler(handler);
				logo.setContentID("logo");
				multipart.addBodyPart(logo);
			} catch (IOException e) {
				e.printStackTrace();
			}

			message.setContent(multipart);

			Transport.send(message);
			logger.info("Sending mail, subject: {} ", subject);

		} catch (MessagingException e) {
			logger.warn("Cannot send mail", e);
			return;
		}
	}
}
