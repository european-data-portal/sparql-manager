package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

@Named
@SessionScoped
public class HeaderFooterBean implements Serializable {

	@Inject
	private transient Logger logger;

	@Inject
	private LanguageBean languageBean;

	private static final long serialVersionUID = 1L;

	private String searchterm = "";

	public String getSearchterm() {
		return searchterm;
	}

	public void setSearchterm(String searchterm) {
		this.searchterm = searchterm;
	}

	public void search() throws IOException {
		// TODO remove string literals
		String redirect = "/" + languageBean.getLocaleCode() + "/search/site/"
				+ this.searchterm;
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		externalContext.redirect(redirect);
	}
}
