package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

@Named
@ApplicationScoped
public class FacesMessageBean {

	/**
	 * 
	 */
	@Inject
	private transient Logger logger;

	public void addMessage(Severity severity, String componentId,
			String messageString) {

		logger.debug("componentId: {}", componentId);

		FacesMessage message = new FacesMessage(messageString);
		message.setSeverity(severity);
		FacesContext context = FacesContext.getCurrentInstance();

		// String componentClientId = null;
		// if (componentId != null) {
		// componentClientId = context.getViewRoot()
		// .findComponent(componentId).getClientId();
		// logger.debug("componentClientId: {} ", componentClientId);
		// }

		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(componentId, message);
	}
}
