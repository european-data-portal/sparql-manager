package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

import com.hp.hpl.jena.sparql.resultset.ResultsFormat;

import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;

/**
 * This class implements methods for downloading and previewing SPARQL-Results.
 * 
 * @author fma
 *
 */
@Named
@SessionScoped
public class DownloadBean implements Serializable {

	private Map<String, String> availableAceModes;
	{
		// LinkedHashMap: insertion order.
		// TreeMap: automatic sorting by map key.
		// HashMap: unordered.
		Map<String, String> aAceModes = new LinkedHashMap<String, String>();
		aAceModes.put(ResultsFormat.FMT_RDF_XML.toString(), "xml");
		aAceModes.put(ResultsFormat.FMT_RDF_TURTLE.toString(), "jsoniq");
		aAceModes.put(ResultsFormat.FMT_RS_CSV.toString(), "text");
		aAceModes.put(ResultsFormat.FMT_RS_JSON.toString(), "json");
		aAceModes.put(ResultsFormat.FMT_RS_XML.toString(), "xml");
		availableAceModes = Collections.unmodifiableMap(aAceModes);
	}

	private Map<String, String> availableFileExtensions;
	{
		// LinkedHashMap: insertion order.
		// TreeMap: automatic sorting by map key.
		// HashMap: unordered.
		Map<String, String> aFileExtensions = new LinkedHashMap<String, String>();
		aFileExtensions.put(null, "");
		aFileExtensions.put(ResultsFormat.FMT_RDF_XML.toString(), ".xml");
		aFileExtensions.put(ResultsFormat.FMT_RDF_TURTLE.toString(), ".json");
		aFileExtensions.put(ResultsFormat.FMT_RS_CSV.toString(), ".csv");
		aFileExtensions.put(ResultsFormat.FMT_RS_JSON.toString(), ".json");
		aFileExtensions.put(ResultsFormat.FMT_RS_XML.toString(), ".xml");
		availableFileExtensions = Collections.unmodifiableMap(aFileExtensions);
	}

	/**
	 * this comment is added to the truncated preview.
	 */
	private static final String PREVIEW_TRUNCATE_COMMENT = "\n<!-- ... -->";

	/**
	 * 
	 */
	@Inject
	private transient Logger logger;

	@Inject
	private SystemConfiguration systemConfiguration;

	private static final long serialVersionUID = 1L;

	private int resultPreviewLength;

	@PostConstruct
	public void postConstruct() {
		resultPreviewLength = Integer.parseInt(
				systemConfiguration.getProperties().getProperty(SparqlManagerServiceConfigKeys.RESULT_PREVIEW_LENGTH));
	}

	/**
	 * Initiates the result download.
	 * 
	 * @param result
	 *            the SPARQL result
	 */
	public void download(final Result result) {

		if (result != null && result.getData() != null) {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();

			String filename = "result-" + result.getResultId() + availableFileExtensions.get(result.getResultFormat());

			logger.warn("filename={}", filename);

			ec.responseReset();
			// Some JSF component library or some Filter might have set some
			// headers
			// in the buffer beforehand.
			// We want to get rid of them, else it may collide.
			ec.setResponseContentType(MediaType.APPLICATION_XML);
			// Check http://www.iana.org/assignments/media-typesfor all types.
			// Use
			// if necessary ExternalContext#getMimeType() for auto-detection
			// based
			// on filename.
			ec.setResponseContentLength(result.getData().length);
			// Set it with the file size. This header is optional. It will work
			// if
			// it's omitted, but the download progress will be unknown.
			ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
			// The Save As popup magic is done here. You can give it any file
			// name
			// you want, this only won't work in MSIE, it will use current
			// request
			// URL as file name instead.

			OutputStream output;
			try {
				output = ec.getResponseOutputStream();

				// Now you can write the InputStream of the file to the above
				// OutputStream the usual way.
				// ...
				IOUtils.copy(new ByteArrayInputStream(result.getData()), output);

			} catch (IOException e) {
				logger.warn("Download failed for {}", filename);
			} finally {
				fc.responseComplete(); // Important! Otherwise JSF will attempt
										// to
				// render the response which obviously will fail
				// since it's already written with a file and
				// closed.
			}
		}
	}

	/**
	 * Generates the result preview.
	 * 
	 * @param result
	 *            the SPARQL result
	 * @return truncated preview
	 */
	public String preview(final Result result) {
		String preview = "";

		if (result != null) {
			String data = new String(result.getData(), StandardCharsets.UTF_8);
			if (data != null) {
				if (data.length() <= resultPreviewLength) {
					preview = data;
				} else {
					preview = data.substring(0, resultPreviewLength) + PREVIEW_TRUNCATE_COMMENT;
				}
			}
		}
		return preview;
	}

	public String extension(final Result result) {
		String preview = "";

		if (result != null) {
			String data = result.getResultFormat();
			if (data != null) {
				if (data.length() <= resultPreviewLength) {
					preview = data;
				} else {
					preview = data.substring(0, resultPreviewLength) + PREVIEW_TRUNCATE_COMMENT;
				}
			}
		}
		return preview;
	}

	/**
	 * Looks up a suitable file Ace mode for the result.
	 * 
	 * @param result
	 *            the SPARQL result
	 * @return truncated preview
	 */
	public String aceMode(final Result result) {
		if (result != null) {
			return this.availableAceModes.get(result.getResultFormat());
		}
		return null;
	}
}
