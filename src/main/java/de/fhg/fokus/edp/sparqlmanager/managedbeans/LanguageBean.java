package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;
import java.util.Map;


@Named
@RequestScoped
public class LanguageBean implements Serializable {

    private String localeCode;
    private String lang = "en";
    private Locale locale;


    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        lang = params.get("lang");
        if(lang==null){
            lang = "en";
        }
        locale = new Locale(lang);
        localeCode = locale.getLanguage();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(lang));

    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }
}
