package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import de.fhg.fokus.edp.sparqlmanager.service.CrudServiceBean;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class QueriesBean implements Serializable {

	@Inject
	private CrudServiceBean crudServiceBean;

	@Inject
	private ProfileBean profileBean;

	@Inject
	private LanguageBean languageBean;

	@Inject
	private transient Logger logger;

	private List<Query> queries;

	private List<Query> publicQueries;

	private static final long serialVersionUID = 1L;

	private float resultLimit = 5;

	private int firstLimit;

	private int secondLimit;

	private int maxPublic;

	private int maxPrivate;

	private  List<Integer> pageNumbersPublic = new ArrayList<Integer>();

	private  List<Integer> pageNumbersPrivate = new ArrayList<Integer>();

	@PostConstruct
	private void init() {
		logger.debug("@PostConstruct");


		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		maxPublic = (int)Math.ceil(crudServiceBean.countAllPubQueries() / resultLimit);
		maxPrivate = (int)Math.ceil(crudServiceBean.countAllQueries(profileBean.getUserId()) / resultLimit);


		if (params.get("secondLimit")==null ){
			secondLimit = 0;
		}else{
			if(Integer.parseInt(params.get("secondLimit")) > maxPrivate){
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("/sparql-manager/" + languageBean.getLang() + "/queries");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			secondLimit = Integer.parseInt(params.get("secondLimit"));
		}

		for (int i = 0; i < maxPrivate; i++){
			pageNumbersPrivate.add(i);
		}

		if(profileBean.getUserId()!= null) {
			queries = crudServiceBean.getAllQueries((int)resultLimit,(int)(secondLimit*resultLimit),profileBean.getUserId());
		}

		/*PUBLIC QUERIES*/

		if (params.get("firstLimit")==null ){
			firstLimit = 0;
		}else{
			if(Integer.parseInt(params.get("firstLimit")) > maxPublic){
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("/sparql-manager/" + languageBean.getLang() + "/queries");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			firstLimit = Integer.parseInt(params.get("firstLimit"));
		}

		for (int i = 0; i < maxPublic; i++){
			pageNumbersPublic.add(i);
		}
		publicQueries = crudServiceBean.getAllPubQueries((int)resultLimit,((int)(firstLimit*resultLimit)));
	}

	public List<Query> getQueries() {
		return queries;
	}

	public List<Query> getPublicQueries() {
		return publicQueries;
	}

	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}

	public int getFirstLimit() {
		return firstLimit;
	}

	public void setFirstLimit(int firstLimit) {
		this.firstLimit = firstLimit;
	}

	public int getSecondLimit() {
		return secondLimit;
	}

	public void setSecondLimit(int secondLimit) {
		this.secondLimit = secondLimit;
	}

	public int getMaxPublic() {
		return maxPublic;
	}

	public void setMaxPublic(int maxPublic) {
		this.maxPublic = maxPublic;
	}

	public int getMaxPrivate() {
		return maxPrivate;
	}

	public void setMaxPrivate(int maxPrivate) {
		this.maxPrivate = maxPrivate;
	}

	public List<Integer> getPageNumbersPublic() {
		return pageNumbersPublic;
	}

	public void setPageNumbersPublic(List<Integer> pageNumbersPublic) {
		this.pageNumbersPublic = pageNumbersPublic;
	}

	public List<Integer> getPageNumbersPrivate() {
		return pageNumbersPrivate;
	}

	public void setPageNumbersPrivate(List<Integer> pageNumbersPrivate) {
		this.pageNumbersPrivate = pageNumbersPrivate;
	}
}
