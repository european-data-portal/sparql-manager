/**
 * Provides the classes necessary to create a injectable logger instance.
 */
package de.fhg.fokus.edp.sparqlmanager.logger;