/**
 * Provides the classes necessary to perform CAS/ECAS authentication.
 */
package de.fhg.fokus.edp.sparqlmanager.auth;