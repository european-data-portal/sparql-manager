package de.fhg.fokus.edp.sparqlmanager.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public interface GenericCrudService {
	public Serializable create(Serializable t);

	public Serializable find(Serializable id, Class type);

	public void delete(Serializable t);

	public void delete(Serializable id, Class typ);

	public Serializable update(Serializable t);

	public Collection<Serializable> findByNamedQuery(String namedQueryName);

	public int countNamedQuery(String namedQueryName);

	public Collection<Serializable> findByNamedQuery(String namedQueryName, int resultLimit, int firstLimit);

	public int countNamedQuery(String namedQueryName,String userId);

	public Collection<Serializable> findByNamedQuery(String namedQueryName, int resultLimit, int firstLimit,String userId);

	public Collection<Serializable> findByNamedQuery(String namedQueryName, Map<String, Object> parameters);

	public Collection<Serializable> findByNamedQuery(String namedQueryName, Map<String, Object> parameters,
			int resultLimit);
}
