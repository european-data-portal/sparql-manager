package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import com.hp.hpl.jena.sparql.resultset.ResultsFormat;
import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;
import de.fhg.fokus.edp.sparqlmanager.service.jena.SparqlClient;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;
import org.ocpsoft.rewrite.faces.navigate.Navigate;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Named
@SessionScoped
public class SearchBean implements Serializable {

	@Inject
	private transient Logger logger;

	@Inject
	SparqlClient sparqlClient;

	@Inject
	SystemConfiguration systemConfiguration;

	@Inject
	FacesMessageBean facesMessageBean;

    @Inject
    LanguageBean languageBean;

	private String sparqlprefixes = "";

	@SuppressWarnings("unused")
	private final String AVAILABLE_CATEGORIES_SPARQL = "SELECT ?theme (count(?theme) AS ?count) WHERE { ?s a dcat:Dataset . ?s dcat:theme ?theme } GROUP BY ?theme ORDER BY DESC(?count)";

	@SuppressWarnings("unused")
	private final String AVAILABLE_FORMATS_SPARQL = "SELECT ?format (count(?format) AS ?count) WHERE { ?s a dcat:Dataset . ?s dcat:distribution ?distribution. ?distribution dct:format ?format } GROUP BY ?format ORDER BY DESC(?count)";

	private final String FILTER_PLACEHOLDER = "#FILTER# \n";

	private final String SELECT_PLACEHOLDER = "#SELECT#";

	private final String LIMIT_PLACEHOLDER = "#LIMIT#";

	private final String FIND = "SELECT " + SELECT_PLACEHOLDER
			+ " WHERE { ?s a dcat:Dataset " + FILTER_PLACEHOLDER + " } "
			+ LIMIT_PLACEHOLDER;

	private final String DEFAULT_RESULTS_FORMAT = ResultsFormat.FMT_RDF_XML
			.toString();

	private String rawquery = "";

	// private String sparqlquery = ""; // =
	// "select (count(*) as ?count) where { ?s a dcat:Dataset }";

	private Query query;

	private Result result = null;

	int RESULT_PREVIEW_LENGHT = 1024;

	private String searchString = "";
	private Boolean title = false;
	private Boolean description = false;
	private Boolean count = false;
	private String formatString = "";

	private int limit = 100;

	private Map<String, String> availableResultFormats;
	{
		// LinkedHashMap: insertion order.
		// TreeMap: automatic sorting by map key.
		// HashMap: unordered.
		Map<String, String> aResultFormats = new LinkedHashMap<String, String>();
		aResultFormats.put(ResultsFormat.FMT_RDF_XML.toString(), "RDF/XML");
		aResultFormats.put(ResultsFormat.FMT_RDF_TURTLE.toString(),
				"RDF/TURTLE");
		aResultFormats.put(ResultsFormat.FMT_RS_CSV.toString(), "RS/CSV");
		aResultFormats.put(ResultsFormat.FMT_RS_JSON.toString(), "RS/JSON");
		aResultFormats.put(ResultsFormat.FMT_RS_XML.toString(), "RS/XML");
		setAvailableResultFormats(Collections.unmodifiableMap(aResultFormats));
	}

	private String[] categories = {};

	private Map<String, String> availableCategories;

	private Collection<String> formats;

	private Map<String, String> availableFormats;

	private String DCATAP_FILETYPES_PREFIX;

	private String DCATAP_FILETYPES;

	private String DCATAP_CATEGORIES_PREFIX;

	private String DCATAP_CATEGORIES;

	private int ASSISTANT_LIST_LENGTH;

	private int ASSISTANT_LIST_LENGTH_COLLAPSED = 3;

	private int listLength = ASSISTANT_LIST_LENGTH;

	private Boolean listExpanded = true;

	private static final long serialVersionUID = 1L;

	public void preRender() {

	}

	@PostConstruct
	public void postConstruct() {

		this.query = new Query();
		this.query.setResultFormat(DEFAULT_RESULTS_FORMAT);

		sparqlprefixes = systemConfiguration.getProperties().getProperty(
				SparqlManagerServiceConfigKeys.APACHE_JENA_PREFIXES);

		RESULT_PREVIEW_LENGHT = Integer.parseInt(systemConfiguration
				.getProperties().getProperty(
						SparqlManagerServiceConfigKeys.RESULT_PREVIEW_LENGTH));

//		DEFAULT_QUERY_ID = Integer.parseInt(systemConfiguration.getProperties()
//				.getProperty(SparqlManagerServiceConfigKeys.DEFAULT_QUERY_ID));
//
//		DEFAULT_RESULT_ID = Integer.parseInt(systemConfiguration
//				.getProperties().getProperty(
//						SparqlManagerServiceConfigKeys.DEFAULT_RESULT_ID));

		DCATAP_FILETYPES_PREFIX = systemConfiguration.getProperties()
				.getProperty(
						SparqlManagerServiceConfigKeys.DCATAP_FILETYPES_PREFIX);

		DCATAP_FILETYPES = systemConfiguration.getProperties().getProperty(
				SparqlManagerServiceConfigKeys.DCATAP_FILETYPES);

		ASSISTANT_LIST_LENGTH = Integer.parseInt(systemConfiguration
				.getProperties().getProperty(
						SparqlManagerServiceConfigKeys.ASSISTANT_LIST_LENGTH));

		ASSISTANT_LIST_LENGTH_COLLAPSED = Integer
				.parseInt(systemConfiguration
						.getProperties()
						.getProperty(
								SparqlManagerServiceConfigKeys.ASSISTANT_LIST_LENGTH_COLLAPSED));

		initAvailableFormats();

		DCATAP_CATEGORIES_PREFIX = systemConfiguration
				.getProperties()
				.getProperty(
						SparqlManagerServiceConfigKeys.DCATAP_CATEGORIES_PREFIX);

		DCATAP_CATEGORIES = systemConfiguration.getProperties().getProperty(
				SparqlManagerServiceConfigKeys.DCATAP_CATEGORIES);

		initAvailableCategories();
	}

	private void initAvailableCategories() {
		availableCategories = new TreeMap<String, String>();
		String[] categoryPairs = DCATAP_CATEGORIES.split(";");

		for (int i = 0; i < categoryPairs.length; i++) {
			String pair = categoryPairs[i];
			String[] keyValue = pair.split(":");

			if (keyValue.length == 2)
				availableCategories.put(DCATAP_CATEGORIES_PREFIX + keyValue[0],
						keyValue[1]);
			else
				logger.warn("Invalid entry: {} ", keyValue.toString());
		}
	}

	private void initAvailableFormats() {
		availableFormats = new LinkedHashMap<String, String>();
		String[] pairs = DCATAP_FILETYPES.split(",");
		for (int i = 0; i < pairs.length; i++) {
			String pair = pairs[i];
			String[] keyValue = pair.split(":");
			if (keyValue.length == 2)
				availableFormats.put(keyValue[0], keyValue[1]);
			else
				logger.warn("Invalid entry: {} ", keyValue.toString());
		}
	}

	public Navigate search() {
		String data = null;
		try {
			if (query.getQueryString() != null
					&& !query.getQueryString().isEmpty())
				data = sparqlClient.runQuery(query.getQueryString(),
						query.getResultFormat());
			if (data != null && !data.isEmpty()) {
				result = new Result();
				result.setResultFormat(query.getResultFormat());
				result.setData(data.getBytes(StandardCharsets.UTF_8));
			}
		} catch (Exception e) {
			facesMessageBean.addMessage(FacesMessage.SEVERITY_ERROR, "searchForm:sparqlquery",
					e.getMessage());
			logger.debug("Error during sparqlquery '{}'",
					query.getQueryString(), e);
		}

        Map<String,String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String lang = params.get("lang");

		if(FacesContext.getCurrentInstance().getExternalContext().getRequest().toString().contains("assistant")){
			return Navigate.to("/" + lang + "/assistant");
		}

		if(FacesContext.getCurrentInstance().getExternalContext().getRequest().toString().contains("query")){
			String id = params.get("queryId");
			return Navigate.to("/" + lang + "/query/" + id);
		}

		return Navigate.to("/" + lang + "/");
	}

	public String[] getCategories() {
		return categories;
	}

	public void setCategories(String[] categories) {
		this.categories = categories;
	}

	class Category {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	public void updateQuery(final String query) {
		this.rawquery = query;
		replaceQuery();
	}

	public String updateLimit(final String query) {
		return query.replaceAll(LIMIT_PLACEHOLDER, "LIMIT " + limit);
	}

	public void updateQuery() {
		this.rawquery = this.FIND;
		replaceQuery();
	}

	public void replaceQuery() {

		String sparqlquery = rawquery;

		if (count)
			sparqlquery = sparqlquery.replaceAll(SELECT_PLACEHOLDER,
					"(count(*) as ?count)");
		else
			sparqlquery = sparqlquery.replaceAll(SELECT_PLACEHOLDER, " * ");

		if (title && searchString != null && !searchString.isEmpty())
			sparqlquery = sparqlquery
					.replaceAll(FILTER_PLACEHOLDER,
							" . \n ?s dct:title ?title . FILTER regex(?title, \""
									+ searchString + "\", \"i\") "
									+ FILTER_PLACEHOLDER);
		if (description && searchString != null && !searchString.isEmpty())
			sparqlquery = sparqlquery
					.replaceAll(FILTER_PLACEHOLDER,
							". \n ?s dct:description ?description . FILTER regex(?description, \""
									+ searchString + "\", \"i\") "
									+ FILTER_PLACEHOLDER);

		if (categories != null && categories.length > 0) {
			String categoriesFilter = "";
			for (String cat : categories) {
				categoriesFilter = categoriesFilter + ". \n ?s dcat:theme "
						+ "<" + cat + ">";
			}
			sparqlquery = sparqlquery.replaceAll(FILTER_PLACEHOLDER,
					categoriesFilter + " " + FILTER_PLACEHOLDER);
		}

		if ((formats != null && formats.size() > 0)
				|| (formatString != null && !formatString.isEmpty())) {
			String formatsFilter = ". \n ?s dcat:distribution ?distribution"
					+ ". \n ?distribution dct:format ?format"
					+ ". \n ?format rdf:type dct:MediaTypeOrExtent";

			if (formats != null && formats.size() > 0)
				for (String format : formats) {
					formatsFilter = formatsFilter + ". \n ?format rdfs:label \""
							+ format + "\" ";
				}

			if (formatString != null && !formatString.isEmpty())
				formatsFilter = formatsFilter
						+ ". \n ?format rdfs:label ?label"
						+ ". \n FILTER regex(?label, \"" + formatString
						+ "\", \"i\") ";

			sparqlquery = sparqlquery.replaceAll(FILTER_PLACEHOLDER,
					formatsFilter + " " + FILTER_PLACEHOLDER);
		}

		sparqlquery = updateLimit(sparqlquery);

		// remove remainig placeholders
		sparqlquery = sparqlquery.replaceAll(FILTER_PLACEHOLDER, "");

		query.setQueryString(sparqlquery);
	}

	public String getSparqlprefixes() {
		return sparqlprefixes;
	}

	public void setSparqlprefixes(String sparqlprefixes) {
		this.sparqlprefixes = sparqlprefixes;
	}

	public Boolean getTitle() {
		return title;
	}

	public void setTitle(Boolean title) {
		this.title = title;
	}

	public Boolean getDescription() {
		return description;
	}

	public void setDescription(Boolean description) {
		this.description = description;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public Map<String, String> getAvailableFormats() {
		return availableFormats;
	}

	public void setAvailableFormats(Map<String, String> availableFormats) {
		this.availableFormats = availableFormats;
	}

	public Map<String, String> getAvailableCategories() {
		return availableCategories;
	}

	public void setAvailableCategories(Map<String, String> availableCategories) {
		this.availableCategories = availableCategories;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public Boolean getCount() {
		return count;
	}

	public void setCount(Boolean count) {
		this.count = count;
	}

	public String getFormatString() {
		return formatString;
	}

	public void setFormatString(String formatString) {
		this.formatString = formatString;
	}

	public Collection<String> getFormats() {
		return formats;
	}

	public void setFormats(Collection<String> formats) {
		this.formats = formats;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public Boolean getListExpanded() {
		return listExpanded;
	}

	public void setListExpanded(Boolean listExpanded) {
		this.listExpanded = listExpanded;
	}

	public void expandCategoriesAndFormats(Boolean expand) {

		this.listExpanded = expand;

		if (listExpanded)
			this.listLength = this.ASSISTANT_LIST_LENGTH;
		else
			this.listLength = this.ASSISTANT_LIST_LENGTH_COLLAPSED;
		this.initAvailableCategories();
		this.initAvailableFormats();
	}

	public String getRawquery() {
		return rawquery;
	}

	public void setRawquery(String rawquery) {
		this.rawquery = rawquery;
	}

	public Map<String, String> getAvailableResultFormats() {
		return availableResultFormats;
	}

	public void setAvailableResultFormats(
			Map<String, String> availableResultFormats) {
		this.availableResultFormats = availableResultFormats;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}
}