package de.fhg.fokus.edp.sparqlmanager.logger;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logging producer for injectable logger.
 */
@Dependent
public class LoggerProducer {
	/**
	 * @param injectionPoint
	 *            the injection point
	 * @return logger Logger
	 */
	@Produces
	public final Logger produceLogger(final InjectionPoint injectionPoint) {
		return LoggerFactory.getLogger(injectionPoint.getMember()
				.getDeclaringClass().getName());
	}
}