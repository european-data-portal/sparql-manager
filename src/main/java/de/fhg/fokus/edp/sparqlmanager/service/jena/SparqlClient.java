package de.fhg.fokus.edp.sparqlmanager.service.jena;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.hp.hpl.jena.sparql.resultset.ResultsFormat;

import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;

@ApplicationScoped
public class SparqlClient {

	@Inject
	SystemConfiguration systemConfiguration;

	@Inject
	private transient Logger logger;

	private final ResultsFormat defaultResultsFormat = ResultsFormat.FMT_RDF_XML;

	private Map<String, ResultsFormat> availableResultFormats;
	{
		Map<String, ResultsFormat> aResultFormats = new HashMap<String, ResultsFormat>();
		aResultFormats.put(ResultsFormat.FMT_RDF_XML.toString(),
				ResultsFormat.FMT_RDF_XML);
		aResultFormats.put(ResultsFormat.FMT_RS_JSON.toString(),
				ResultsFormat.FMT_RS_JSON);
		aResultFormats.put(ResultsFormat.FMT_RDF_TURTLE.toString(),
				ResultsFormat.FMT_RDF_TURTLE);
		aResultFormats.put(ResultsFormat.FMT_RS_CSV.toString(),
				ResultsFormat.FMT_RS_CSV);
		aResultFormats.put(ResultsFormat.FMT_RS_XML.toString(),
				ResultsFormat.FMT_RS_XML);
		availableResultFormats = Collections.unmodifiableMap(aResultFormats);
	}

	public String runQuery(String queryString, String resultsFormatString) {
		ResultsFormat resultsFormat;
		resultsFormat = availableResultFormats.get(resultsFormatString);
		if (resultsFormat == null) {
			logger.warn("no lookup possible for resultformatstring: {}",
					resultsFormatString);
			resultsFormat = defaultResultsFormat;
		}
		return runQuery(queryString, resultsFormat);
	}

	public String runQuery(String queryString) {
		return runQuery(queryString, defaultResultsFormat);
	}

	public String runQuery(String queryString, ResultsFormat resultsFormat) {

		String prefixes = systemConfiguration.getProperties().getProperty(
				SparqlManagerServiceConfigKeys.APACHE_JENA_PREFIXES);

		String prefixQueryString = prefixes + " " + queryString;

		String sparqlUrl = systemConfiguration.getProperties().getProperty(
				SparqlManagerServiceConfigKeys.SPARQL_URL);

		String sparqlGraphIri = systemConfiguration.getProperties()
				.getProperty(SparqlManagerServiceConfigKeys.SPARQL_GRAPH_IRI);

		Model model = ModelFactory.createDefaultModel();

		Query query = QueryFactory.create(prefixQueryString
		/* , Syntax.syntaxSPARQL_11 */);

		QueryEngineHTTP httpQuery = new QueryEngineHTTP(sparqlUrl, query);

		int queryType = query.getQueryType();

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		logger.debug("sparqlquery '{}'", prefixQueryString);

		switch (queryType) {
		case Query.QueryTypeAsk:
			boolean b = httpQuery.execAsk(); // Result that has to be
												// formatted
			ResultSetFormatter.outputAsTSV(byteArrayOutputStream, b);
			break;
		case Query.QueryTypeConstruct:
			model = httpQuery.execConstruct(); // Result that has to be
												// formatted
			model.write(byteArrayOutputStream);
			break;
		case Query.QueryTypeDescribe:
			model = httpQuery.execDescribe(); // Result that has to be
												// formatted
			model.write(byteArrayOutputStream);
			break;
		case Query.QueryTypeSelect:
			ResultSet results = ResultSetFactory.copyResults(httpQuery
					.execSelect()); // Result that has to be formatted
			ResultSetFormatter.output(byteArrayOutputStream, results,
					resultsFormat);
			break;
		}

		httpQuery.close();

		try {
			return byteArrayOutputStream
					.toString(StandardCharsets.UTF_8.name()).trim();
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			unsupportedEncodingException.printStackTrace();
		}

		return null;
	}
}
