package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import de.fhg.fokus.edp.sparqlmanager.messages.MessageProvider;
import de.fhg.fokus.edp.sparqlmanager.service.CrudServiceBean;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;
import de.fhg.fokus.edp.sparqlmanager.service.model.User;
import org.ocpsoft.rewrite.faces.navigate.Navigate;
import org.slf4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class ResultBean implements Serializable {

	@Inject
	private CrudServiceBean crudServiceBean;

	@Inject
	private transient Logger logger;

	@Inject
	private ProfileBean profileBean;

	@Inject
	FacesMessageBean facesMessageBean;

	@Inject
	private LanguageBean languageBean;

	@Inject
	SystemConfiguration systemConfiguration;

	@Inject
	private MessageProvider messageProvider;

	private long resultId;
	private long queryId;

	private Result result;
	private Query query;

	private boolean authenticated = false;

	private static final long serialVersionUID = 1L;

	public Navigate loadResult() {

		for (Query q : crudServiceBean.getAllQueries()) {
			if (q.getQueryId() == queryId) {
				query = q;
				if (profileBean.getUserId()!=null){
					User user = crudServiceBean.getUser(profileBean.getUserId());
					for(Query quer : user.getQueries()){
						if(query.getQueryId()==quer.getQueryId()){
							authenticated = true;
						}
					}
				}
				for (Result r : query.getResults()) {
					if (r.getResultId() == resultId) {
						result = r;
						break;
					}
				}
			}
		}
		if(isAuthenticated()==false && query.isPublicised()==false){

			facesMessageBean.addMessage(FacesMessage.SEVERITY_INFO, null,
					messageProvider.getValue("SPARQLMANAGER.MESSAGE.SEARCH_ERROR"));

			return Navigate.to("/" + languageBean.getLang().toString() + "/queries");
		}
		return null;
	}

	public long getQueryId() {
		return queryId;
	}

	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public Long getResultId() {
		return resultId;
	}

	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public Navigate deleteResult() {
		crudServiceBean.deleteResult(queryId, resultId);
		return Navigate.to("/" + languageBean.getLang().toString() + "/query/" + queryId);
	}
}
