package de.fhg.fokus.edp.sparqlmanager.navigation;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

import javax.servlet.ServletContext;

/**
 *
 * Adds a REST like Infrastructure on a Prettyfaces Implemenatation // http://www.ocpsoft.org/java/migrating-from-prettyfaces-to-rewrite-simplicity-meets-power/
 */

@RewriteConfiguration
public class ApplicationNavigationConfigurationProvider extends HttpConfigurationProvider {


    @Override
    public Configuration getConfiguration(ServletContext servletContext) {
        return ConfigurationBuilder.begin()
                .addRule(Join.path("/{lang}/").to("/index.xhtml"))
                .addRule(Join.path("/{lang}/assistant").to("/assistant.xhtml"))
                .addRule(Join.path("/{lang}/queries/{firstLimit}/{secondLimit}").to("/queries.xhtml?{firstLimit}?{secondLimit}"))
                .addRule(Join.path("/{lang}/queries").to("/queries.xhtml"))
                .addRule(Join.path("/{lang}/query/new").to("/query.xhtml?queryId=0"))
                .addRule(Join.path("/{lang}/query").to("/query.xhtml"))
                .addRule(Join.path("/{lang}/result").to("/result.xhtml"))
                .addRule(Join.path("/{lang}/user/{profileBean.domainUsername}").to("/user.xhtml"))
                .addRule(Join.path("/{lang}/user").to("/user.xhtml"))
              //  .addRule(Join.path("/{lang}/login").to("/login.xhtml"))
                .addRule(Join.path("/{lang}/query/{queryId}").to("/query.xhtml?{queryId}"))
                .addRule(Join.path("/{lang}/result/{resultId}/{queryId}").to("/result.xhtml?{resultId}&{queryId}"))
                ;



    }

    @Override
    public int priority() {
        return 0;
    }
}


/**
 * OLD PRETTYCONFIG.XML
 *
 * <pretty-config xmlns="http://ocpsoft.org/schema/rewrite-config-prettyfaces"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://ocpsoft.org/schema/rewrite-config-prettyfaces
 http://ocpsoft.org/xml/ns/prettyfaces/rewrite-config-prettyfaces.xsd">

 <url-mapping id="root">
 <pattern value="/#{languageBean.localeCode}/" />
 <view-id value="/" />
 </url-mapping>

 <url-mapping id="index">
 <pattern value="/#{languageBean.localeCode}/index.xhtml" />
 <view-id value="/index.xhtml" />
 </url-mapping>

 <url-mapping id="assistant">
 <pattern value="/#{languageBean.localeCode}/assistant.xhtml" />
 <view-id value="/assistant.xhtml" />
 </url-mapping>

 <url-mapping id="queries">
 <pattern value="/#{languageBean.localeCode}/queries.xhtml" />
 <view-id value="/queries.xhtml" />
 </url-mapping>
 </pretty-config>

 */