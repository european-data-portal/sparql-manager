package de.fhg.fokus.edp.sparqlmanager.service;

import de.fhg.fokus.edp.sparqlmanager.scheduler.PeriodicTimer;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;
import de.fhg.fokus.edp.sparqlmanager.service.model.User;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.*;

@ApplicationScoped
public class CrudServiceBean {

	@Inject
	private GenericCrudService genericCrudService;

	@EJB
	private PeriodicTimer periodicTimer;

	@Inject
	private transient Logger logger;

	public List<User> getUsers() {
		Collection<Serializable> users = genericCrudService.findByNamedQuery("User.findAll");
		return new ArrayList(users);
	}

	public User addUser(User user) {
		User u = (User) genericCrudService.create(user);
		return u;
	}

	public User getUser(String userId) {
		User u = (User) genericCrudService.find(userId, User.class);
		return u;
	}

	public User editUser(User user) {
		User u = (User) genericCrudService.update(user);
		return u;
	}

	public boolean deleteUser(String userId) {
		// TODO error handling
		genericCrudService.delete(userId, User.class);
		return true;
	}

	public Query addQuery(String userId, Query query) {
		logger.debug("addQuery({},{})", userId, query);
		User u = getUser(userId);
		u.getQueries().add(query);
		// genericCrudService.create(query);
		genericCrudService.update(u);
		try {
			if (query.isEnabled())
				periodicTimer.addTimer(query);
		} catch (Exception e) {
			logger.error("timer creation failed for query " + query.getQueryId() + " with schedule string '"
					+ query.getScheduleString() + "'");
		}
		return query;
	}

	public List<Query> getAllQueries() {
		Collection<Serializable> queries = genericCrudService.findByNamedQuery("Query.findAll");
		return new ArrayList(queries);
	}

	public int countAllPubQueries() {
		int max = genericCrudService.countNamedQuery("Query.countAllPublics");
		return max;
	}

	public List<Query> getAllPubQueries(int resultLimit, int firstLimit) {
		Collection<Serializable> queries = genericCrudService.findByNamedQuery("Query.findAllPublics",resultLimit,firstLimit);
		return new ArrayList(queries);
	}

	public int countAllQueries(String userId) {
		int max = genericCrudService.countNamedQuery("User.countQuerys",userId);
		return max;
	}

	public List<Query> getAllQueries(int resultLimit, int firstLimit, String userId) {
		Collection<Serializable> queries = genericCrudService.findByNamedQuery("User.findQuerys",resultLimit,firstLimit,userId);
		return new ArrayList(queries);
	}

	public List<Query> getQueries(String userId) {
		User u = getUser(userId);
		if (u != null)
			return u.getQueries();
		return null;
	}

	public Query getQuery(Long queryId) {
		Query q = (Query) genericCrudService.find(queryId, Query.class);
		return q;
	}

	public Query editQuery(Query query) {
		genericCrudService.update(query);
		periodicTimer.deleteTimer(query);
		if (query.isEnabled())
			periodicTimer.addTimer(query);
		return query;
	}

	public Boolean deleteQuery(String userId, Long queryId) {
		logger.debug("deleteQuery({},{})", userId, queryId);
		User u = getUser(userId);
		Query q = getQuery(queryId);
		periodicTimer.deleteTimer(q);
		u.getQueries().remove(q);
		genericCrudService.update(u);
		genericCrudService.delete(q);
		return true;
	}

	public List<Result> getResults(Long queryId) {
		Query q = getQuery(queryId);
		if (q != null)
			return q.getResults();
		return null;
	}

	public Result addResult(Long queryId, Result result) {
		Query q = getQuery(queryId);
		q.getResults().add(result);
		// genericCrudService.create(result);
		genericCrudService.update(q);
		return result;
	}

	public Result getResult(Long resultId) {
		Result r = (Result) genericCrudService.find(resultId, Result.class);
		return r;
	}

	public Boolean deleteResult(Long queryId, Long resultId) {
		try {
			Query q = getQuery(queryId);
			Result r = getResult(resultId);
			q.getResults().remove(r);
			genericCrudService.update(q);
			genericCrudService.delete(r);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}

	public Result getLastResult(Long queryId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("queryId", queryId);
		Collection<Serializable> lastResults = genericCrudService.findByNamedQuery("Result.findLastByQueryId",
				parameters, 1);

		if (lastResults.size() > 0) {
			// compare to previous results
			Result lastResult = (Result) lastResults.toArray()[0];
			return lastResult;
		}
		return null;
	}
}