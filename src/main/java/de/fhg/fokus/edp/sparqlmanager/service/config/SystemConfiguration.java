package de.fhg.fokus.edp.sparqlmanager.service.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class SystemConfiguration {

	@Inject
	private transient Logger logger;

	private Properties props;

	@PostConstruct
	public void init() {
		InputStream in;
		props = new Properties();
		in = this.getClass().getResourceAsStream(
				"/sparql-manager-service.properties");
		try {
			props.load(in);
		} catch (IOException e) {
			logger.error("Can not load properties file", e);
		}
	}

	public Properties getProperties() {
		return props;
	}
}
