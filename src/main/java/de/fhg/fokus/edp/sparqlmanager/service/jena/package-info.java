/**
 * Provides the classes necessary to access a SPARQL endpoint.
 */
package de.fhg.fokus.edp.sparqlmanager.service.jena;