/**
 * Provides the managed bean classes.
 */
package de.fhg.fokus.edp.sparqlmanager.managedbeans;