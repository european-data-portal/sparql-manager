/**
 * Provides the classes necessary to access property files.
 */
package de.fhg.fokus.edp.sparqlmanager.service.config;