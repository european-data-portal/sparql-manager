/*
package de.fhg.fokus.edp.sparqlmanager.auth;

import org.pac4j.cas.client.CasClient;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.config.ConfigFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ClientConfigFactory implements ConfigFactory {

	public Config build() {

		// TODO use dependency injection
		String path = "sparql-manager-service.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = loader.getResourceAsStream(path);

		Properties props = new Properties();
		try {
			props.load(inputStream);
		} catch (IOException e) {
			// TODO exception logging
		}

		String casLoginUrl = props.getProperty("cas.login.url");

		String casCallbackUrl =props.getProperty("cas.callback.url");

		// CAS
		final CasClient casClient = new CasClient();
		casClient.setName("CasClient");
		//casClient.setGateway(true);
		casClient.setCasLoginUrl(casLoginUrl);

		final Clients clients = new Clients(casCallbackUrl, casClient);

		final Config config = new Config(clients);

		return config;
	}
}
*/
