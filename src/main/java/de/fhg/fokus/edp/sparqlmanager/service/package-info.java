/**
 * Provides the classes for CRUD services.
 */
package de.fhg.fokus.edp.sparqlmanager.service;