package de.fhg.fokus.edp.sparqlmanager.service.config;

public interface SparqlManagerServiceConfigKeys {

	String SPARQL_URL = "sparql.url";
	String SPARQL_GRAPH_IRI = "sparql.graph.iri";
	String SPARQL_REFERENCE_URI = "sparql.reference.uri";

	String RESULT_PREVIEW_LENGTH = "result.preview.length";

	String APACHE_JENA_PREFIXES = "apache.jena.prefixes";

	String DCATAP_FILETYPES_PREFIX = "dcatap.filetypes.prefix";
	String DCATAP_FILETYPES = "dcatap.filetypes";
	String DCATAP_CATEGORIES_PREFIX = "dcatap.categories.prefix";
	String DCATAP_CATEGORIES = "dcatap.categories";

	String ASSISTANT_LIST_LENGTH = "assistant.list.length";
	String ASSISTANT_LIST_LENGTH_COLLAPSED = "assistant.list.length.collapsed";
	
	String CAS_LOGIN_URL = "cas.login.url";
	String CAS_CALLBACK_URL = "cas.callback.url";
	
	String REPLY_EMAIL = "reply.email";
	
	String MY_QUERIES_FEATURE_ENABLED = "myqueries.feature.enabled";
}