package de.fhg.fokus.edp.sparqlmanager.managedbeans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;

import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;

/**
 * This class implements easy access to the service configuration.
 * 
 * @author fma
 *
 */
@Named
@ApplicationScoped
public class ConfigBean implements Serializable {

	/**
	 * 
	 */
	@Inject
	private transient Logger logger;

	@Inject
	private SystemConfiguration systemConfiguration;

	private static final long serialVersionUID = 1L;

	private boolean myQueriesFeatureEnabled;

	@PostConstruct
	public void postConstruct() {
		myQueriesFeatureEnabled = Boolean
				.parseBoolean(systemConfiguration
						.getProperties()
						.getProperty(
								SparqlManagerServiceConfigKeys.MY_QUERIES_FEATURE_ENABLED));

		logger.debug(SparqlManagerServiceConfigKeys.MY_QUERIES_FEATURE_ENABLED
				+ "={}", myQueriesFeatureEnabled);
	}

	public boolean isMyQueriesFeatureEnabled() {
		return myQueriesFeatureEnabled;
	}

	public void setMyQueriesFeatureEnabled(boolean myQueriesFeatureEnabled) {
		this.myQueriesFeatureEnabled = myQueriesFeatureEnabled;
	}
}
