package de.fhg.fokus.edp.sparqlmanager.scheduler;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.slf4j.Logger;

import de.fhg.fokus.edp.sparqlmanager.managedbeans.ConfigBean;
import de.fhg.fokus.edp.sparqlmanager.service.CrudServiceBean;
import de.fhg.fokus.edp.sparqlmanager.service.Mail;
import de.fhg.fokus.edp.sparqlmanager.service.config.SparqlManagerServiceConfigKeys;
import de.fhg.fokus.edp.sparqlmanager.service.config.SystemConfiguration;
import de.fhg.fokus.edp.sparqlmanager.service.jena.SparqlClient;
import de.fhg.fokus.edp.sparqlmanager.service.model.Query;
import de.fhg.fokus.edp.sparqlmanager.service.model.Result;
import de.fhg.fokus.edp.sparqlmanager.service.model.User;

@Singleton
@Startup
public class PeriodicTimer {

	@Resource
	private TimerService timerService;

	@EJB
	private Mail mail;

	@Inject
	private SparqlClient sparqlClient;

	@Inject
	private CrudServiceBean crudServiceBean;

	@Inject
	private transient Logger logger;

	@Inject
	private ConfigBean configBean;

	@Inject
	private SystemConfiguration systemConfiguration;

	private String url;

	private final int SECOND = 0;
	private final int MINUTE = 1;
	private final int HOUR = 2;
	private final int DAY_OF_WEEK = 3;
	private final int DAY_OF_MONTH = 4;
	private final int MONTH = 5;
	private final int YEAR = 6;

	@PostConstruct
	private void postConstruct() {
		boolean myQueriesFeatureEnabled = configBean.isMyQueriesFeatureEnabled();
		if (myQueriesFeatureEnabled) {
			List<User> users = crudServiceBean.getUsers();
			for (User user : users) {
				List<Query> queries = user.getQueries();
				for (Query query : queries) {
					if (query.isEnabled())
						addTimer(query);
				}
			}
		}

		// TODO add new systemconfiguration property
		url = systemConfiguration.getProperties().getProperty(SparqlManagerServiceConfigKeys.CAS_CALLBACK_URL);
		url = url.replaceAll("callback", "");
	}

	public void addTimer(Query q) {
		if (q != null) {
			ScheduleExpression expression = this.getExpression(q.getScheduleString());
			timerService.createCalendarTimer(expression, new TimerConfig(q.getQueryId(), false));
			logger.debug("timer created for query {}", q.getQueryId());
		}
	}

	public void modifyTimer(Query q) {
		deleteTimer(q);
		addTimer(q);
	}

	public void deleteTimer(Query q) {
		if (q != null) {
			Collection<Timer> timers = timerService.getAllTimers();
			for (Timer timer : timers) {
				long timerQueryId = (long) timer.getInfo();
				if (timerQueryId == q.getQueryId()) {
					timer.cancel();
					logger.debug("timer canceled for query [{}]", q.getQueryId());
				}
			}
		}
	}

	public Date getTimeout(Query q) {
		if (q != null) {
			Collection<Timer> timers = timerService.getAllTimers();
			for (Timer timer : timers) {
				long timerQueryId = (long) timer.getInfo();
				if (timerQueryId == q.getQueryId()) {
					return timer.getNextTimeout();
				}
			}
		}
		return null;
	}

	private ScheduleExpression getExpression(String scheduleExpressionString) {

		ScheduleExpression expression = new ScheduleExpression();
		List<String> scheduleExpressionList = Arrays.asList(scheduleExpressionString.split(" "));
		try {
			expression.year(scheduleExpressionList.get(YEAR));
			expression.month(scheduleExpressionList.get(MONTH));
			expression.dayOfMonth(scheduleExpressionList.get(DAY_OF_MONTH));
			expression.dayOfWeek(scheduleExpressionList.get(DAY_OF_WEEK));
			expression.hour(scheduleExpressionList.get(HOUR));
			expression.minute(scheduleExpressionList.get(MINUTE));
			expression.second(scheduleExpressionList.get(SECOND));
		} catch (Exception e) {
			logger.warn("Cannot determine timer config from '{}', using default", scheduleExpressionString);
		}
		logger.debug("Using timer config: '{}'", expression.toString());
		return expression;
	}

	@Timeout
	public void timeout(Timer timer) {
		long queryId = (long) timer.getInfo();

		try {
			timeout(queryId);

		} catch (Exception e) {
			logger.warn("errror executing query timer: ", e);
			logger.warn("canceling timer");
			timer.cancel();
			Query query = crudServiceBean.getQuery(queryId);
			if (query == null)
				return;
			query.setEnabled(false);
			crudServiceBean.editQuery(query);
		}

	}

	public void timeout(long queryId) {

		Query query = crudServiceBean.getQuery(queryId);
		if (query == null)
			return;

		logger.debug("executing query {}", query.toString());

		String xmlResult = sparqlClient.runQuery(query.getQueryString(), query.getResultFormat());

		Result lastResult = crudServiceBean.getLastResult(queryId);
		String lastResultData = null;
		if (lastResult != null)
			lastResultData = new String(lastResult.getData(), StandardCharsets.UTF_8);

		boolean resultChanged = resultHasChanged(lastResultData, xmlResult);

		resultChanged = true;
		if (resultChanged) {
			logger.debug("result has changed: {}", resultChanged);
			Result result = new Result();
			result.setData(xmlResult.getBytes(StandardCharsets.UTF_8));

			crudServiceBean.addResult(queryId, result);

			// TODO add multilanguage email template
			mail.send(query.getNotificationString(), "New results for query '" + query.getQueryName() + "'",
					"New results for query '" + query.getQueryName() + "' \nView the result: " + url
							+ "result.xhtml?resultId=" + crudServiceBean.getLastResult(queryId).getResultId()
							+ "&queryId=" + query.getQueryId());
		}
	}

	public Boolean resultHasChanged(String lastData, String newData) {
		if (lastData == null)
			return true;
		else if (newData == null)
			return false;
		else
			return !lastData.equals(newData);
	}

}
