package de.fhg.fokus.edp.sparqlmanager;

/**
 * Created by lli on 12.01.2017.
 */
public final class ExtraFunctions {

    private ExtraFunctions(){

    }

    public static String cutIt(String text, Integer index){
        if(text==null || text.length()==0) return "";
        if(text.length()<=index) return text;
        return text.substring(0,index) + "...";
    }

}
