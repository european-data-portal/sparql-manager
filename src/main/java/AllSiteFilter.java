import org.slf4j.Logger;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by lli on 18.10.2016.
 */

public class AllSiteFilter implements Filter {

    @Inject
    private transient Logger logger;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (req.getRequestURI().contains("png") || req.getRequestURI().contains("europeandataportal") || req.getRequestURI().contains("assistant") || req.getRequestURI().contains("login") || req.getRequestURI().contains("result") || req.getRequestURI().contains("query")|| req.getRequestURI().contains("queries") || req.getRequestURI().contains("javax") || req.getRequestURI().length() <= 19 || req.getRequestURI().contains("index")) {
            request.getRequestDispatcher(((HttpServletRequest) request).getServletPath()).forward(req, res);
        } else {
            chain.doFilter(req, res);
            return;
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {
    }
}