import de.fhg.fokus.edp.sparqlmanager.managedbeans.ProfileBean;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class KeycloakLogoutFilter implements Filter {

    @Inject
    private ProfileBean profileBean;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        request.setAttribute("org.keycloak.KeycloakSecurityContext", null);
        profileBean.setAuthenticated(false);
        request.getSession().invalidate();
        request.logout();
        HttpServletResponse httpResponse = (HttpServletResponse) resp;
        //httpResponse.sendRedirect(req.getLocale() + "/?GLO=true");
        //TODO hardcoded redirect uri!
        httpResponse.sendRedirect("https://www.ppe-aws.europeandataportal.eu/auth/realms/edp/protocol/openid-connect/logout?redirect_uri=https%3A%2F%2Fwww.ppe-aws.europeandataportal.eu%2Fsparql-manager%2Fen%2F");

    }

    public void init(FilterConfig config) throws ServletException {

    }


}
