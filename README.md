# SPARQL Manager for the pan-European Data Portal

The SPARQL manager is responsible for sending user defined SPARQL queries on demand or at a scheduled time to Virtuosos SPARQL query engine. It receives and stores the result. Additionally it sends a notification to the user.

## Installation for development

### Prerequisite

Install all the following software:

 * Oracle / OpenJDK JDK >= 1.8
 * Apache Maven >= 3.2
 * PostgreSQL >= 9.2
 * JBoss WildFly (Java EE7 Full & Web Distribution) >= 10.x
 * GIT >= 1.9.4 
 
### Clone the sources
 
Run the following command to clone the repository:

    git clone git@gitlab.com:european-data-portal/sparql-manager.git
    
### Setup your system

Start your installed WildFly and go to http://localhost:9990/console. Follow the instructions on that site to create a management user for WildFly. If you want to access WildFly from another machine like localhost, you have to configurate the interfaces: https://docs.jboss.org/author/display/WFLY10/Interfaces+and+ports
Create or edit your Maven settings in ~/.m2/settings.xml. Insert the following XML:

    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
       http://maven.apache.org/xsd/settings-1.0.0.xsd">
    
         <localRepository>${user.home}/.m2/repository</localRepository>
            <interactiveMode>true</interactiveMode>
            <usePluginRegistry>false</usePluginRegistry>
            <offline>false</offline>
        
            <profiles>
                <profile>
                    <id>sparql-manager</id>
                    <properties>
                        <wildfly-hostname>localhost</wildfly-hostname>
                        <wildfly-port>9990</wildfly-port>
                        <wildfly-username>USERNAME</wildfly-username>
                        <wildfly-password>PASSWORD</wildfly-password>
                        <hibernate.hbm2ddl.auto>update</hibernate.hbm2ddl.auto>
                        <triplestore.url>https://triplestore.url/sparql/</triplestore.url>
                        <triplestore.graph>https://europe.eu/paneodp/</triplestore.graph>
                        <triplestore.reference.uri>https://europeandataportal.eu/</triplestore.reference.uri>
                        <javax.faces.PROJECT_STAGE>Development</javax.faces.PROJECT_STAGE>
                        <cas.login.url>https://cas.login.url/login</cas.login.url>
                        <cas.callback.url>http://cas.callback.url/callback</cas.callback.url>
                        <reply.email>noreply@reply.email</reply.email>
                        <myqueries.feature.enabled>true</myqueries.feature.enabled>
                    </properties>
                </profile>
            </profiles>
        
            <activeProfiles>
                <activeProfile>sparql-manager</activeProfile> <!-- This ensures that maven is using the sparql-manager profile with each run -->
            </activeProfiles>
    </settings>
    
### Datasources

Configure a Datasource with the following JNDI Name

    java:jboss/datasources/sparql-manager
	
### Mail Connector

Configure a Mail Connector with the following JNDI Name

    java:jboss/mail/sparql-manager
    
### Adjust Tables in PostgresDB

Currently there are extra columns created in the tables query and result which will need to be deleted.
For the use psql or some tool to alter the tables and drop userid in query and queryid in result table

    ALTER TABLE query DROP COLUMN userid; 
    ALTER TABLE result_ DROP COLUMN queryid;

### Generate model classes

Generate model classes with JAXB and persistence annotations seperately

	mvn generate-sources

### Deployment Wildfly

Deploy to your Wildfly instance

	mvn wildfly:deploy

### Start page

Visit the start page

	 http://localhost:8080/sparql-manager/en/	
